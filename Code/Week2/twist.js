'use strict';

var gl;
var points = [];
var newPoints = [];
var rotationAngle = 0;
var count = 5;

window.onload = function () {
  var canvas = document.getElementById('gl-canvas');
  var realToCSSPixels = window.devicePixelRatio || 1;

  var angleRange  = document.getElementById('angleRange'),
      divideRange = document.getElementById('divideRange');

  angleRange.addEventListener('change', function (event) {
    var self  = this,
        value = parseInt(self.value);
    rotationAngle = value * Math.PI / 180;
    init();
  });

  divideRange.addEventListener('change', function (event) {
    var self = this;
    count = parseInt(self.value);
    init();
  });

  function init() {
    // 1.
    gl = WebGLUtils.setupWebGL(canvas);
    if (!gl) { alert('Code isn\'t available'); }

    var displayWidth = Math.floor(gl.canvas.clientWidth * realToCSSPixels);
    var displayHeight = Math.floor(gl.canvas.clientHeight * realToCSSPixels);

    var vertices = [
      vec2(-0.5, -0.5),
      vec2(0, 0.5),
      vec2(0.5, -0.5)
    ];

    points = [];
    newPoints = [];
    divideTriangle(vertices[0], vertices[1], vertices[2], count);
    for (var i = 0; i < points.length; i++) {
      twist(points[i], rotationAngle);
    }

    // 2.
    gl.canvas.width = displayWidth;
    gl.canvas.height = displayHeight;
    gl.viewport(10, 10, gl.canvas.width - 20, gl.canvas.height - 20);
    gl.clearColor(0.0, 0.0, 0.0, 1.0);

    // 3.
    var program = initShaders(gl, 'vertex-shader', 'fragment-shader');
    gl.useProgram(program);

    // 4.
    var bufferId = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, bufferId);
    gl.bufferData(gl.ARRAY_BUFFER, flatten(newPoints), gl.STATIC_DRAW);

    // 5.
    var vPosition = gl.getAttribLocation(program, 'vPosition');
    gl.vertexAttribPointer(vPosition, 2, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(vPosition);

    render();
  }

  init();
};

function render() {
  gl.clear(gl.COLOR_BUFFER_BIT);
  //gl.drawArrays(gl.TRIANGLES, 0, 3);
  for (var i = 0; i < points.length; i += 3) {
    gl.drawArrays(gl.LINE_LOOP, i, 3);
  }
}

function triangle(a, b, c) {
  points.push(a, b, c);
}

function divideTriangle(a, b, c, count) {
  //                       c
  //                      /\
  //                     /  \
  //                  a /____\ b
  //
  if (count === 0) {
    triangle(a, b, c);
  } else {
    var ab = mix(a, b, 0.5);
    var bc = mix(b, c, 0.5);
    var ac = mix(a, c, 0.5);
    --count;
    divideTriangle(a, ab, ac, count);
    divideTriangle(b, ab, bc, count);
    divideTriangle(c, ac, bc, count);
    divideTriangle(ac, ab, bc, count);
  }
}

function twist(point, angle, distance) {
  var x = point[0], y = point[1];
  distance = distance || Math.sqrt(x * x + y * y);
  var distanceAngle = distance * angle;
  var newPoint = [];
  newPoint[0] = x * Math.cos(distanceAngle) - y * Math.sin(distanceAngle);
  newPoint[1] = x * Math.sin(distanceAngle) + y * Math.cos(distanceAngle);
  newPoints.push(newPoint);
}